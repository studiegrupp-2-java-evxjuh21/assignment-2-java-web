package com.example.assignment2javaweb.entities;

import javax.persistence.*;

@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String activity;

    @Column
    private int priority;

    public Todo(String activity, int priority) {
        this.activity = activity;
        this.priority = priority;
    }

    public Todo() {

    }

    public int getId() {
        return id;
    }

    public String getActivity() {
        return activity;
    }

    public int getPriority() {
        return priority;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
