package com.example.assignment2javaweb.controllers;

import com.example.assignment2javaweb.entities.Todo;
import com.example.assignment2javaweb.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/todo")
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public String getAllTodos(Model model) {
        List<Todo> todoList = todoService.findAll()
                .stream()
                .sorted(Comparator.comparing(Todo::getPriority))
                .toList();

        model.addAttribute("todoList", todoList);
        return "index";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTodoById(@PathVariable("id") int id) {
        todoService.deleteById(id);

        return ResponseEntity.status(303).header("Location", "/todo").build();
    }

    @PostMapping
    public String createTodo(@ModelAttribute Todo todo) {
        todoService.save(todo);
        return "redirect:/todo";
    }

}
