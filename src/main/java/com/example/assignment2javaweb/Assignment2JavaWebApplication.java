package com.example.assignment2javaweb;

import com.example.assignment2javaweb.entities.Todo;
import com.example.assignment2javaweb.repositories.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class Assignment2JavaWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2JavaWebApplication.class, args);
    }

}
